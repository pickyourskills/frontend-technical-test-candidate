import React, { Component } from "react";
import { Router, Switch, Route } from "react-router-dom";
import { SingleContainer } from "./SingleContainer";
import { AllPostsContainer } from "./AllPostsContainer";
import { history } from "./helpers";
import "semantic-ui/dist/semantic.min.css";

class App extends Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          <Route path="/post/:post_id" component={SingleContainer} />
          <Route path="/" component={AllPostsContainer} />
        </Switch>
      </Router>
    );
  }
}

export default App;
