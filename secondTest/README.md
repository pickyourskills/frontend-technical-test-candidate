Avec l'api https://jsonplaceholder.typicode.com/
et avec pour point de départ l'app présente dans ce dossier:
Le but de ce test est avant tout de nous montrer de quoi vous êtes capable en React et la propreté de votre code. Vous êtes libre de réorganiser/renommer tout les fichiers/dossier
Aucune limitation, vous pouvez utiliser tout se que vous voulez

L'objectif est de créer un mini reddit. Les requêtes d'api ont https://jsonplaceholder.typicode.com/ pour base

1) Il nous faut un Header avec le titre de notre site, et un Footer avec un peu de texte au choix. 
Le footer et le header doivent être "sticky", cad qu'ils doivent être toujours visible, même si l'on scroll. Le header doit contenir un lien en forme d'icône vers la page /

2) La page / doit contenir tous les posts retournés par l'API à /posts. 
Les posts seront présentés sous forme de liste avec le nom de l'auteur et son titre.

3) En cliquant sur le post, on est redirigé vers post/post_id. Cette page doit contenir le titre du post,son auteur et le contenu du post. 
Le nom de l'auteur est un lien vers sa page de profil. Tous les commentaires liés au post sont présenté en dessous du post

4) La page de profil d'un utilisateur doit comporter le plus d'infos possible, ainsi qu'une liste de tous ses posts.
 
NB:
1) RTFM
2) Google is your friend
3) Faire un petit npm install avant tout
4) npm start pour lancer le serveur de dev
5) Pour renvoyer le test, rm -rf tout les nodes modules, puis uploader sur wetransfer.


Bonus: 
Réaliser l'application avec les react-hooks
Il y a un bug avec les nested ressources de l'api. Quel est t-il et trouve l'issue github.
Pour le design, nous conseillons le framework React SemanticUI (https://react.semantic-ui.com/) qui est déjà installé, mais tu peux utiliser un autre framework bien entendu.


