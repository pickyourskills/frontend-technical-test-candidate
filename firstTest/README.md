Test d'algorithmique
Le but de ce test est de nous montrer votre capacité à écrire des fonctions lisibles et efficientes tout en utilisant les dernières possibilités en Javascript (Ecma[5,6,7])
Dans un premier temps, tu peux faire une des 3 fonctions suivantes, psuis passer sur l'exercice 2 qui est plus important, puis revenir sur ces fonctions s'il te reste du temps.

Les fichiers des fonctions sont dans ./src
Chaque fonction doit être écrite dans son fichier imposé.

Pour debugger vos fonctions et voir les console.log, vous pouvez lancer la commande suivante:
node <path_of_file>
Pour tester que tout marche vous pouvez faire npm test


1 - Écrire une fonction en JS qui prend en argument un array et filtre les éléments non uniques.
Nom de fichier: function1.js

Exemple:
function([1, 2, 3, 3, 4]) doit renvoyer [1, 2, 4]

2 - Écrire une fonction JS qui filtre les valeurs spécifiées d'un tableau. La fonction doit retourner un tableau sans les valeurs passées en argument.
Les arguments de la fonction doivent être de la forme suivante: (array, arg1, arg2, arg3, ..., argN)
Nom de fichier: function2.js

Exemple:
function(['a', 'b', 'c', 'b', 'a'], 'a', 'c') doit renvoyer ['b', 'b']

3 - Écrire une fonction JS qui retourne la valeur d'une clé d'un JSON, dont le nombre de niveau est indeterminé.
Si la clé n'existe pas, la fonction doit renvoyer undefined
Nom de fichier: function3.js
Les arguments de la fonction doivent être de la forme: (object, key)

Exemple:
/*
const object = {
  user: {
    user_name: 'Jean',
    location: {
      location_name: 'Paris',
      country: {
        country_name: 'France'
      }
    }
  }
}
*/

function(object, "country_name") doit renvoyer "France"
