import function1 from './function1';
import function2 from './function2';
import function3 from './function3';

test("filter unique [1, 2, 2, 3, 4, 4, 5]", () => {
  expect(function1([1, 2, 2, 3, 4, 4, 5])).toEqual([1, 3, 5]);
});

test("filter unique [1, 2, 3, 4, 5]", () => {
  expect(function1([1, 2, 3, 4, 5])).toEqual([1, 2, 3, 4, 5]);
});

test("pull a,c  from ['a', 'b', 'c', 'a', 'b', 'c']", () => {
  expect(function2(["a", "b", "c", "a", "b", "c"], "a", "c")).toEqual(["b", "b"]);
});

test("pull b  from ['a', 'b', 'c', 'a', 'b', 'c']", () => {
  expect(function2(["a", "b", "c", "a", "b", "c"], "b")).toEqual(["a", "c", "a", "c"]);
});

const data = {
  level1: {
    level2: {
      level3: "some data"
    }
  }
};

test("function3 level3 gives 'some data'", () => {
  expect(function3(data, "level3")).toEqual("some data");
});

test("function3 level4 gives 'undefined'", () => {
  expect(function3(data, "level4")).toEqual(undefined);
});